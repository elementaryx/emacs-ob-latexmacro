;; Handling latex macros so that:
;; - 1. they are used as latex header
;; - 2. they are also used as html head extra
;; - 3. and they get tangled, so that ob-latexpicture can load them as well
;; All credits go to oantolin for 1. and 2. https://www.reddit.com/r/orgmode/comments/7u2n0h/tip_for_defining_latex_macros_for_use_in_both/
;; All credits go to  R. Bosman for inline variant (:inline yes) of 1. and 2. https://emacs.stackexchange.com/questions/54703/exporting-latex-commands-to-html-mathjax

(require 'ox-latex)
(require 'ob-latex)

(add-to-list 'org-src-lang-modes '("latexmacro" . latex))

;; setcustom org-babel-latexmacros-tangle-file
;; (setq org-babel-latexmacros-tangle-file
;;       (concat ".latexmacros-"
;; 	      (lambda () (buffer-name))
;; 	      "-.tex"))

(setq org-babel-default-header-args:latexmacro
      '((:results . "raw")
        (:exports . "results")
	(:tangle . "yes")
	(:inline . "no")))

(defun prefix-all-lines (pre body)
  (with-temp-buffer
    (insert body)
    (string-insert-rectangle (point-min) (point-max) pre)
    (buffer-string)))

(defun org-babel-execute--latexmacro-as-header (body)
  (message "latexmacro: as-header")
  (concat
   (prefix-all-lines "#+LATEX_HEADER: " body)
   "\n#+HTML_HEAD_EXTRA: <div style=\"display: none\"> \\(\n"
   (prefix-all-lines "#+HTML_HEAD_EXTRA: " body)
   "\n#+HTML_HEAD_EXTRA: \\)</div>\n"))

(defun org-babel-execute--latexmacro-inline (body)
  (message "latexmacro: inline")
  (concat
   "\n#+BEGIN_EXPORT latex\n"
   body
   "\n#+END_EXPORT"
   "\n#+BEGIN_EXPORT html\n"
   "<div style=\"display: none\"> \n\\(\n"
   body
   "\n\\)\n</div>\n"
   "#+END_EXPORT"))

(defun org-babel-execute:latexmacro (body params)
  (if (string= "yes" (cdr (assq :inline params)))
      (org-babel-execute--latexmacro-inline body)
    (org-babel-execute--latexmacro-as-header body)))

(defun org-babel-latexmacro-save-current-org-src-source-file-name ()
  (message "buffer-file-name: %s" (buffer-file-name))
  (setq org-babel-latexmacro-current-org-src-source-file-name (buffer-file-name)))

(defun org-babel-latexmacro-get-current-org-src-source-file-name () org-babel-latexmacro-current-org-src-source-file-name)

(setq org-export-before-processing-functions nil)
(add-hook 'org-export-before-processing-functions
	  (lambda (_backend) (org-babel-latexmacro-save-current-org-src-source-file-name)))
(add-hook 'org-export-before-processing-functions
	  (lambda (_backend) (org-babel-tangle nil nil "latexmacro")))

(provide 'ob-latexmacro)
